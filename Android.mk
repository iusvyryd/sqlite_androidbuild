LOCAL_PATH:= $(call my-dir)

include $(LOCAL_PATH)/build-config.mk

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
  src/sqlite3.c \
  src/shell.c

ifneq ($(TARGET_ARCH),arm)
  LOCAL_LDLIBS += -ldl
endif

LOCAL_CFLAGS += $(sqlite_cflags)
LOCAL_MODULE := sqlite3

include $(BUILD_SHARED_LIBRARY)
