sqlite-AndroidBuild
--------------------

Building [Sqlite] for android.
Only for Linux hosts.

Building:

`cd sqlite-androidbuild`

`NDK=/path/to/android/ndk sh build.sh`

to build for specific ABI (default ABI=all) specify:

`ABI=armeabi-v7`

[Sqlite]:  http://www.sqlite.org/